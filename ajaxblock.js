var ajaxblock_included_scripts=new Array();

function ajaxblock_do_transition(effect, fromElement, toElement, callback)
{
  eval("ajaxblock_transition_"+effect+"(fromElement, toElement, callback)");
}

function ajaxblock_block_info(block)
{
  var classes = block.attr('class').split(/\s+/);  
  var id = block.attr('id').substring(6);  
  
  var delta = null;
  var module = null;
  
  for(var i in classes)
  {
      var cl = classes[i];
      if(cl.substring(0, 6) == "block-" && cl != "block-ajax-enabled")
      {
	  module = cl.substring(6);
	  delta = id.substring(module.length + 1);
	  break;
      }
  }
  
  return {module:module, delta:delta};
}

function ajaxblock_load_block(block, url, method, d)
{
    var preloaderIcon = jQuery("<div class=\"icon\"></div>");
    var preloader = jQuery("<div class=\"ajaxblock-preloader\"></div>");
    preloader.hide();
    preloaderIcon.width(block.outerWidth());
    preloaderIcon.height(block.outerHeight());
    preloader.append(preloaderIcon);
    jQuery(block).prepend(preloader);    
    
    preloader.show();
  
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1]; 
    if(additionalURL == undefined)
	additionalURL = "";
    var temp = "";

    var tempArray = additionalURL.split("&");
    if(tempArray.length > 1)
    for ( var i in tempArray )
    {
      if(tempArray[i].indexOf("ajaxreq") == -1)
      {
	newAdditionalURL += temp+tempArray[i];
                temp = "&";
      }
    }
    
    var info = ajaxblock_block_info(block);
    
    url = temp+"ajaxreq="+info.module+":"+info.delta;
    url = baseURL+"?"+newAdditionalURL+url;
    
    function replaceScripts(scripts)
    {
	var localScripts = [];
      
	jQuery("head script").each(function(){ // Scan our local scripts
	    var src = jQuery(this).attr("src");
	    if(src == undefined)
	      return;
	    
	    var srcClean = src.split("?")[0].split("&")[0];
	    localScripts.push(srcClean);
	});
	
	jQuery(scripts).each(function(){
	    var src = jQuery(this).attr("src");
	    if(src == undefined)
	      return;
	    
	    var srcClean = src.split("?")[0].split("&")[0];
	    if(jQuery.inArray(srcClean, localScripts) < 0) // New script found
	    {
	      jQuery("head").append(this);
	    }
	});
    }
                
    function ajaxSuccess(data)
    {
	for(var block in data.fx)
	{
	    var id = "#block-" + data.fx[block].module + "-" + data.fx[block].delta;
	    var localblock = jQuery(id);
	    var ajaxblock = jQuery(id, data.page);
	    
	    //var headerScripts = jQuery("head", data.page);
	    //alert(headerScripts.length);
	    replaceScripts(data.scripts);

	    var fx = data.fx[block].fx;
	    
	    
	    ajaxblock_do_transition(fx, localblock, ajaxblock, function(el) 
	    {
		   preloader.remove();
		   ajaxblock_setup(el);
	    });
	    
	}
    }
        
    jQuery.ajax({
     type: "POST",
     url: url,
     data: d,
     success:ajaxSuccess
      });
}

function ajaxblock_setup(block)
{
    
     //       block.css("border", "1px solid red");
     var target=null;
  
   
   jQuery("form", jQuery(">.content", block)).submit(function(ev){     
     
	    var t = jQuery(this);
	    if(t.valid != undefined && !t.valid()) // jQuery.validation
		return;
     
	    var url = jQuery(this).attr("action");
	    var method = jQuery(this).attr("method");
	    var d = jQuery(this).serializeArray();
	    	    
	    jQuery("*", block).attr("disabled", "disabled");
	    
	    console.debug(jQuery(this).data("events"));
	    console.debug(ev);
	    
	    d.push({name: target.attr("name"), value: target.attr("value")});
	    	    
 	    ajaxblock_load_block(block, url, method, d);
	    	    	    
	    return false;
      });
   
    jQuery("form input[type='submit']", block).click(function(){
	target=jQuery(this);
	//return false;
    });
}

jQuery(document).ready(function(){
  jQuery(".block-ajax-enabled>.content").each(function(){
      var block = jQuery(this).parent();
      
      ajaxblock_setup(block);
  });
  
});
