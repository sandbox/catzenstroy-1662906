function ajaxblock_transition_slideright(oldElement, newElement, finishedCallback)
{
    var pos = oldElement.position();
    var width = oldElement.outerWidth();
    var height = oldElement.outerHeight();
  
    var tmp = jQuery("<div></div>");
        
    tmp.css({
	/*position: "absolute",
	top: pos.top + "px",
	left: pos.left + "px",
	"z-index": 100
	*/
	width: width + "px",
	height: height + "px",
	"margin-top": oldElement.css("margin-top"),
	"margin-left": oldElement.css("margin-left"),
	"margin-bottom": oldElement.css("margin-bottom"),
	"margin-right": oldElement.css("margin-right"),
	 overflow:"hidden",
	 position:"relative"
    });
    	
    //tmp.insertBefore(oldElement);
    oldElement.replaceWith(tmp);
    
    oldElement.css({
        "position":"absolute",
	 width: width + "px",
	 height: height + "px",
	 "z-index":9998,
    });
   
    var newWrapper = jQuery("<div></div>");
    newWrapper.css({
	position:"absolute",
	width: width + "px",
	"z-index":9998,
    });
         
    newWrapper.append(newElement);
    
    tmp.append(newWrapper);
    tmp.append(oldElement);
    
    var rightToLeft = false;
    
    if(rightToLeft)
    {
      
      newWrapper.css({"left": width + "px", "z-index":9999});
    
	tmp.animate({"height": newElement.outerHeight()}, 500, function(){
	oldElement.animate({"left": -width}, 500);
	
	newWrapper.animate({"left": 0}, 500, function(){
	  tmp.replaceWith(newElement);
	  finishedCallback(newElement);
	});
    });
    
    }
    
    else
    {
      newWrapper.css("left", (-width) + "px");
      oldElement.css("z-index", 9999);
    
      tmp.animate({"height": newElement.outerHeight()}, 500, function(){
	oldElement.animate({"left": width}, 500);
	
	newWrapper.animate({"left": 0}, 500, function(){
	  tmp.replaceWith(newElement);
	  finishedCallback(newElement);
	});
    });
    }
    
    
    
    
   // oldElement.parent().prepend(tmp);
    
    
    
    
  //  oldElement.animate({"height" : newElement.height()})
   // finishedCallback();
}
